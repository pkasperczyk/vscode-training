# Szkolenie Visual Studio Code

## Pobranie z internetu

Udajemy się na na oficjalną stronę  [VS Code](https://code.visualstudio.com/) i pobieramy wersję na swoją platformę i instalujemy


## Otwarcie projektu
Pobieramy przykładowy projekt (np. ten z instrukcją z tego szkolenia) za pomocą git'a
```
git clone git@gitlab.com:pkasperczyk/vscode-training.git
```
Otwieramy VS Code i z menu *File/Open Folder* wybieramy katalog projektu

Klikamy na nazwę brancha w lewym dolnym rogu i tworzymy sobie własną gałąż np. *pierwsza litera imienia i nazwiska plus rok*. Możemy to też zrobić w konsoli za pomocą komendy (pk to od Paweł Kasperczyk)
```
git checkbout -b pk2017
```

Aby zainstalować potrzebne dependencje wykonujemy komendę
```
npm install
```


## Operacje na katalogach i plikach
* Tworzymy katalog *frontend* a w nim pliki *index.js*, *index.html*

*index.js*
```javascript
import {h, app} from 'hyperapp'

const counterActions = {
  increment: (state, actions) => {
    const newValueJson = { value: state.value+1 };
    return Object.assign(state, newValueJson);
  },
  decrement: (state, actions) => {
      const newValueJson = { value: state.value-1 };
      return Object.assign(state, newValueJson);
  },
  setState: (state, actions, newState) => {return newState}
}

const counterView = (state, actions) =>
<main>
  <h1>Licznik</h1>
  <span>{state.value} </span>
  <input type="button" value="+" onclick={(e) => { e.preventDefault(); actions.increment(); } }></input>
  <input type="button" value="-" onclick={(e) => { e.preventDefault(); actions.decrement(); } }></input>
</main>

app({
  actions: counterActions,
  view: counterView,
  events: {
    load: (state, actions) => {
      actions.setState({value: 2});
    }
  }
})
```
*index.html*
```html
<html></html>
```
* Tworzymy katalog *backend* a w nim pliki *counterGateway.js*, *server.js*

*counterGateway.js*
```javascript
const fs1 = require ("fs");
module.exports = function counterGatewayFactory (fs, fileName) {
  return  {
    getCounts: function () { 
      return new Promise (function (resolve, reject) {
        fs.readFile(fileName, "UTF-8", function (err, data) {
          if (err) { reject (err); }
          else { resolve(data); }
        })
      })
    },
    saveCounts: function (counts) { 
      return new Promise (function (resolve, reject) {
        fs.writeFile(fileName, counts, "UTF-8", function (err, data) {
          if (err) { reject (err); }
          else { resolve("success")}
        })
      })
    },
  }
}
```

*server.js*
```javascript
const express = require("express");
const bodyParser = require('body-parser');
const counterGatewayFactory = require ('./counterGateway');
const fs = require('fs');

const port = 5000;
const filename = "./src/backend/counter.txt"
const app = express();
const counterGateway = counterGatewayFactory(fs, filename);

app.use(bodyParser.json());
app.use(express.static("src/frontend"));
app.get("/api/counter", function (req, res, next) {
  counterGateway.getCounts().then((data) => res.json({value: Number(data) }));
});
app.post("/api/counter", function (req, res, next) {
  var counts = req.body.value;
  counterGateway.saveCounts(counts).then(() => res.json({success: true }));
});

app.listen(port, function (){
  console.log("Aplikacja uruchomiona na porcie "+port);
});
```

* Tworzymy katalog *test/frontend* a w nim plik *indexTest.js* 

*indexTest.js* 
```javascript
const assert = require("assert")
describe("app test", function () {
  it ("initial test should pass", function () {
    assert.ok(true);
  });
});
```
* Zmieniamy nazwę pliku *index.js* na *app.js* oraz pliku *indexTest.js* na *appTest.js*
* Usuwamy plik *index.html* i tworzymy go na nowo. Tym razem jego postać przyjmuje

*index.html*
```html
<html>
  <head>
  </head>
  <body>
    <script src="./bundle.js"></script>
  </body>
</html>
```

* Z katalogu *temp* kopiujemy plik *countersInitialState.txt* do katalogu *backend* i zmieniamy mu nazwę na *counters.txt* 
* Tworzymy katalog *src* i przesuwamy do niego katalogi *backend* i *frontend*
* Commitujemy nasze zmiany za pomocą gita wbudowanego w program. Zróbmy to na 2 kroki. W pierwszym zacommitujmy zmiany odnoszące się do katalogów *backend* i *frontend* pod nazwą *backend i frontend commit*, natomiast w drugim odnoszące się do *test* pod nazwą *test commit*

## Wyszukiwanie
* Wyszukujemy słowo *return*
* Wyszukujemy słowo *return* ale tylko w folderze *src/backend*
* Otwieramy plik *temp/index.html* w przeglądarce. Wyszukujemy słowo *h1* z opcją *files to include* na ustawioną *html*. Zamieniamy na *i* a potem zapisujemy i odświeżamy stronę
* Podglądamy zmiany w git'cie wbudowanym w VS Code i commitujemy je 

## Edycja
* Podświetlanie składni
* Podpowiadanie składni ( na przykładzie *counterGateway.js*, *server.js* + [JSDoc](https://en.wikipedia.org/wiki/JSDoc)
* [Emmet](https://docs.emmet.io/abbreviations/syntax/) ( na przykładzie *index.html* )
* CSS ( na przykładzie *main.css* )
* *package.json*
* Markdown
* Snippety i extensions (na przykładzie *ES6 Mocha Snippets* i *describeAndIt*)
* Multicursor
* Przydatne skróty klawiaturowe (w *temp/keybindings.json*)
* Dodanie zadania budowania aplikacji i podpięcie skrótu klawiaturowego

## Rozszerzenia
* Ogólnie (Do angulara, do innych języków)
* REST Client ( przykład na podstawie pliku *rest.http* )
* GIT History 

## Inne
* Pushowanie z git'a
* Wspomnienie o debugerze
* Zadanie dla chętnych ( za pomocą funkcji *fetch* użytej na *frontendzie* połączyć *frontend* i *backend* ze sobą, tak żeby stan licznika był przechowywany w pliku *backend/counter.txt*, zapisywany do tego pliku przy operacjach *+*/*-* i ładowany na starcie jako początkowa wartość )